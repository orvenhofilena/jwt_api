<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
Route::post('auth/login', ['uses' => 'AuthController@login', 'as' => 'login']);

Route::get('shipping/routes', ['middleware' => 'auth.admin', 'uses' => 'RouteController@getRoutes']);

Route::post('shipping/points', ['middleware' => 'auth.admin', 'uses' => 'PointController@createPoint']);

Route::get('shipping/points', ['middleware' => 'auth.admin', 'uses' => 'PointController@viewPoints']);

Route::get('shipping/points/{id}', ['middleware' => 'auth.admin', 'uses' => 'PointController@readPoint']);

Route::put('shipping/points/{id}', ['middleware' => 'auth.admin', 'uses' => 'PointController@updatePoint']);

Route::delete('shipping/points/{id}', ['middleware' => 'auth.admin', 'uses' => 'PointController@deletePoint']);

Route::get('shipping/routes/fastest/{start}/{stop}', ['middleware' => 'auth.admin', 'uses' => 'RouteController@bestRoute']);
