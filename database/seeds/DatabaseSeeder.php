<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;
use App\User;
use App\Route;
use App\Point;
use App\Path;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(UsersTableSeeder::class);
        User::create([
            'name' => 'admin',
            'email' => 'admin@test.com',
            'password' => Hash::make('admin'),
            'role' => 'admin'
        ]);
        User::create([
            'name' => 'tradera',
            'email' => 'tds@test.com',
            'password' => Hash::make('qwer'),
            'role' => 'client'
        ]);
        Point::create([
            'point_name' => 'A'
        ]);
        Point::create([
            'point_name' => 'B'
        ]);
        Point::create([
            'point_name' => 'C'
        ]);
        Point::create([
            'point_name' => 'D'
        ]);
        Point::create([
            'point_name' => 'E'
        ]);
        Point::create([
            'point_name' => 'F'
        ]);
        Point::create([
            'point_name' => 'G'
        ]);
        Point::create([
            'point_name' => 'H'
        ]);
        Point::create([
            'point_name' => 'I'
        ]);
        Route::create([
            'start' => '1',
            'stop' => '3',
            'time' => '1',
            'cost' => '20'
        ]);
        Route::create([
            'start' => '3',
            'stop' => '2',
            'time' => '1',
            'cost' => '12'
        ]);
        Route::create([
            'start' => '1',
            'stop' => '8',
            'time' => '10',
            'cost' => '1'
        ]);
        Route::create([
            'start' => '1',
            'stop' => '5',
            'time' => '30',
            'cost' => '5'
        ]);
        Route::create([
            'start' => '8',
            'stop' => '5',
            'time' => '30',
            'cost' => '1'
        ]);
        Route::create([
            'start' => '5',
            'stop' => '4',
            'time' => '3',
            'cost' => '5'
        ]);
        Route::create([
            'start' => '4',
            'stop' => '6',
            'time' => '4',
            'cost' => '50'
        ]);
        Route::create([
            'start' => '6',
            'stop' => '7',
            'time' => '40',
            'cost' => '50'
        ]);
        Route::create([
            'start' => '6',
            'stop' => '9',
            'time' => '45',
            'cost' => '50'
        ]);
        Route::create([
            'start' => '7',
            'stop' => '2',
            'time' => '64',
            'cost' => '73'
        ]);
        Route::create([
            'start' => '9',
            'stop' => '2',
            'time' => '65',
            'cost' => '5'
        ]);
    }
}
