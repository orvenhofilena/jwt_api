**Setting up API**

1. In phpmyadmin, create database jwt_api.
2. In root folder, run php artisan migrate & php artisan db:seed.
3. Run php artisan serve to start app.

**Logging in for Authentication**

Login - POST
http::/127.0.0.1:8000/api/auth/login
Content-Type: application/json
Request Body(form-data):
        email: admin@test.com
        password: admin

**Note:** Paste the {token} in Authorization header for all requests.

**Managing Points**

Add Points - POST
http::/127.0.0.1:8000/api/shipping/points
Authorization: Bearer {token}
Content-Type: application/json
Request Body(raw): 
        point_name - name/description of new point


Read Points - GET
http::/127.0.0.1:8000/api/shipping/points
Authorization: Bearer {token}
Content-Type: application/json


Search Point by id - GET
http::/127.0.0.1:8000/api/shipping/points/{id}
Authorization: Bearer {token}
Content-Type: application/json
Parameters:
        {id} - id of the point to search


Update Point - PUT
http::/127.0.0.1:8000/api/shipping/points/{id}
Authorization: Bearer {token}
Content-Type: application/json
Parameters:
        {id} - id of the point to update
Request Body(raw): 
        point_name - new name of point to update


Delete Point by id - DELETE
http::/127.0.0.1:8000/api/shipping/points/{id}
Authorization: Bearer {token}
Content-Type: application/json
Parameters:
        {id} - id of the point to delete


**View Valid Routes**

View Routes - GET
http::/127.0.0.1:8000/api/shipping/routes
Authorization: Bearer {token}
Content-Type: application/json


**Determine best route for given start point & endpoint**

Get Fastest Route - GET
http::/127.0.0.1:8000/api/shipping/routes/fastest/{start}/{stop}
Authorization: Bearer {token}
Content-Type: application/json
Parameters:
        {start} - Value of the starting point (ex. A)
        {stop} - Value of the stopping point (ex. B)