<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Route;

class RouteController extends Controller
{
    // public function getRoute(Request $request)
    // {
    //     $points = DB::table('points')->select(DB::raw('id, point_name'))->get();
    //     $points = json_decode($points, true);
    //     $container = array();
    //     $sub_array = array();
    //     foreach($points as $point){
    //         $routes=DB::table(DB::raw('routes'))->join('points','routes.stop','=','points.id')->select('points.point_name','routes.stop','routes.time','routes.cost')->where('routes.start', '=', $point['id'])->get();
    //         $routes = json_decode($routes, true);            

    //         foreach($routes as $route){
    //             $sub_array[$route['point_name']] = $route['time'];
    //         }
    //         $container[] = [
    //             $point['point_name'] => $sub_array
    //         ];
    //         $sub_array = null;
    //     }
    //     return response()->json($container);
    // }

    public function bestRoute(Request $request, $start, $stop)
    {
        $points = DB::table('points')->select(DB::raw('id, point_name as pname'))->get();
        $points = json_decode($points, true);
        $container = array();
        $sub_array = array();
        foreach($points as $point){
            $routes=DB::table(DB::raw('routes'))->join('points','routes.stop','=','points.id')->select('points.point_name','routes.stop','routes.time','routes.cost')->where('routes.start', '=', $point['id'])->get();
            $routes = json_decode($routes, true);            

            foreach($routes as $route){
                $sub_array[$route['point_name']] = $route['time'];
            }
            $container[$point['pname']] = $sub_array;
            $sub_array = null;
        }

        // $graph = array(
        //     'A' => array('B' => 9, 'D' => 14, 'F' => 7),
        //     'B' => array('A' => 9, 'C' => 11, 'D' => 2, 'F' => 10),
        //     'C' => array('B' => 11, 'E' => 6, 'F' => 15),
        //     'D' => array('A' => 14, 'B' => 2, 'E' => 9),
        //     'E' => array('C' => 6, 'D' => 9),
        //     'F' => array('A' => 7, 'B' => 10, 'C' => 15),
        //     'G' => array(),
        // );
        
        $graph = $container;
        $algorithm = new \Fisharebest\Algorithm\Dijkstra($graph);
        $routes = $algorithm->shortestPaths($start, $stop);
        
        if(count($routes) == 1){
            return response()->json(['fastest route' => $routes[0]]);
        }elseif(count($routes) > 1){
            foreach($routes as $route){
                return response()->json(['fastest route' => $route[0]]);
            }
        }else{
            return response()->json(['message' => 'no possible route']);
        }
    }


    public function getRoutes(Request $request)
    {
        $routes = DB::select(DB::raw('select r.id as route_no, p1.point_name as start, p2.point_name as stop, r.time, r.cost from points p1, points p2, routes r
        where r.start = p1.id and r.stop = p2.id'));
        return response()->json(["valid routes" => $routes]);
    }
}
