<?php

namespace App\Http\Controllers;

use Tymon\JWTAuth\Facades\JWTAuth;
use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function login(Request $request)
    {
        $credentials = $request->only(['email', 'password']);
  
        if (!$token = JWTAuth::attempt($credentials)) {
            return response()->json(['error' => 'invalid credentials'], 401);
        }

        $payload = JWTAuth::getPayload($token);
        $expiry = $payload['exp'];
        return response()->json([
            'token' => $token,
            'expiry' => $expiry
        ], 200);
    }
}
