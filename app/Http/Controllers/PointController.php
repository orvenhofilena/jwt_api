<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Point;

class PointController extends Controller
{
    public function createPoint(Request $request)
    {
        $point_name = $request->point_name;
        if(!isset($point_name)){
            return response()->json(["message" => "new point_name not set"], 400);
        }else{
            if(Point::where('point_name', $point_name)->exists()){
                return response()->json(["message" => "point_name entered already in use"], 400);
            }else{
                $point = new Point;
                $point->point_name = $request->point_name;
                $point->save();
                return response()->json(["message" => "new point created"], 201);
            }
        }
    }

    public function viewPoints()
    {
        $points = Point::get();
        return response()->json(["data" => $points], 200);
    }

    public function readPoint($id)
    {
        if (Point::where('id', $id)->exists()) {
            $point = Point::where('id', $id)->get();
            return response()->json(["data" => $point], 200);
        } else {
            return response()->json(["message" => "no data not found"], 404);
        }
    }

    public function updatePoint(Request $request, $id)
    {
        if (Point::where('id', $id)->exists()) {
            $pointname = $request->point_name;
            if(!isset($pointname)){
                return response()->json(["message" => "new point_name not set"], 400);
            }else{
                if(Point::where('point_name', $pointname)->exists()){
                    return response()->json(["message" => "point_name entered already in use"], 400);
                }else{
                    $point = Point::find($id);
                    $point->point_name = $pointname;
                    $point->save();
                    return response()->json(["message" => "point_name updated"], 200);
                }
            }
        } else {
            return response()->json(["message" => "data not found"], 404);
        }
    }

    public function deletePoint(Request $request, $id)
    {
        if (Point::where('id', $id)->exists()) {
            DB::table('routes')->where('start', '=', $id)->delete();
            DB::table('routes')->where('stop', '=', $id)->delete();
            $point = Point::find($id);
            $point->delete();
            return response()->json(["message" => "point_name deleted"], 200);
        } else {
            return response()->json(["message" => "data not found"], 404);
        }
    }
}
