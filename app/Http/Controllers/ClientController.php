<?php

namespace App\Http\Controllers;

use Tymon\JWTAuth\Facades\JWTAuth;
use Tymon\JWTAuth\Exceptions\JWTException;
use Illuminate\Http\Request;

class ClientController extends Controller
{
    public function handle($request, Closure $next, $roles)
    {
        try {    
            $token = JWTAuth::parseToken();    
            $user = $token->authenticate();
        } catch (TokenExpiredException $e) {      
            return $this->unauthorized('Your token has expired. Please, login again.');
        } catch (TokenInvalidException $e) {
            return $this->unauthorized('Your token is invalid. Please, login again.');
        }catch (JWTException $e) {
            return $this->unauthorized('Please, attach a Bearer Token to your request');
        }
        if ($user && $user->role() === 'client'){
            return $next($request);
        }
    
        return $this->unauthorized();
    }

    private function unauthorized($message = null)
    {
        return response()->json([
            'message' => $message ? $message : 'You are unauthorized to use this service',
            'success' => false
        ], 401);
    }
}
